package org.fasttrackit;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ShoppingCartPage;
import org.fasttrackit.pages.WishlistPage;

public class DemoShopApp {
    private static final String APP_TITLE = "Demo Shop Testing simulator = -";

    public static final String DEMO_SHOP_TITLE = "Demo shop";

    public static void main(String[] args) {
        System.out.println(APP_TITLE);
        MainPage homePage = new MainPage();
       // verifyStaticPage(homePage);
        verifyLoginModal(homePage);
        verifyWishlistPage(homePage);
        verifyShoppingCartPage(homePage);

        String beetleUser = "beetle";
        loginWithUser(homePage, beetleUser);

        //validateLoggedInUser(homePage, beetleUser);
    }


    private static void loginWithUser(MainPage homePage, String beetleUser) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.clickOnUsernameField();
        modal.typeInUsernameField(beetleUser);
        modal.clickOnPasswordField();
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
    }

    private static void verifyWishlistPage(MainPage homePage) {
        homePage.clickOnTheWishlistButton();
        WishlistPage wishlistPage = new WishlistPage();
        wishlistPage.validateThatTheWishlistPageIsDisplayed();
        wishlistPage.clickOnTheLogoButton();
    }

    private static void verifyLoginModal(MainPage homePage) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.validateModalComponents();
        modal.clickOnCloseButton();
        homePage.validateModalIsNotDisplayed();
    }

    private static void verifyShoppingCartPage(MainPage homePage) {
        homePage.clickOnTheShoppingCartIcon();
        ShoppingCartPage shoppingPage = new ShoppingCartPage();
        shoppingPage.validateIncreaseQtyButtonIsDisplayed();
        shoppingPage.validateDecreaseQtyButtonIsDisplayed();
        shoppingPage.validateTrashCanButtonIsDisplayed();
    }

}