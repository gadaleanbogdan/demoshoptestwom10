package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Header {
    private final SelenideElement logoIconUrl = $(".fa-shopping-bag");

    private final SelenideElement shoppingCartIconUrl = $(".fa-shopping-cart");

    private final String wishlistIconUrl;

    private final String greetingsMessage;

    private final SelenideElement signInButton = $(".fa-sign-in-alt");

    private final SelenideElement signOutButton = $(".fa-sign-out-alt");

    public Header() {
        this.wishlistIconUrl = "/wishlist";
        this.greetingsMessage = "Hello guest!";
    }

    public Header(String user) {
        this.wishlistIconUrl = "/wishlist";
        this.greetingsMessage = "Hi " + user + "!";

    }

    /**
     * Getters
     */

    public SelenideElement getLogoIconUrl() {
        return logoIconUrl;
    }

    public SelenideElement getShoppingCartIconUrl() {
        return shoppingCartIconUrl;
    }

    public String getWishlistIconUrl() {
        return wishlistIconUrl;
    }

    public String getGreetingsMessage() {
        return greetingsMessage;
    }

    public SelenideElement getSignInButton() {
        return signInButton;
    }

    /**
     * Actions
     */

    public void clickOnTheShoppingCartIcon() {
        this.shoppingCartIconUrl.click();
    }

    public void clickOnTheLoginButton() {
        System.out.println(" Clicked on the login button.");
        this.signInButton.click();
    }

    public void clickOnTheLogoutButton() {
        this.signOutButton.click();
    }


    public void clickOnTheWishlistButton() {
        System.out.println(" Clicked on the wishlist button. ");
    }

    public void clickOnTheLogoButton() {
        System.out.println(" Return to the home page ");
        this.logoIconUrl.click();
    }
    
}
