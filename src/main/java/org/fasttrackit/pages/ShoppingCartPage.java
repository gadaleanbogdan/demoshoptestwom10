package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ShoppingCartPage {
    private final SelenideElement subPageTitle = $(".text-muted");
    private final SelenideElement increaseQtyButton = $(".fa-plus-circle");
    private final SelenideElement decreaseQtyButton = $(".fa-minus-circle");
    private final SelenideElement trashCan = $(".fa-trash");
    private final SelenideElement cartBadge = $(".shopping_cart_badge");
    private final SelenideElement continueShoppingButton = $(".btn-danger");
    private final SelenideElement checkoutButton = $(".btn-success");

    public String getSubPageTitle() {
        return this.subPageTitle.text();
    }

    public String getCartBadgeQty() {
        return this.cartBadge.text();
    }

    public void clickOnIncreaseQtyButton() {
        this.increaseQtyButton.click();
    }

    public void clickOnDecreaseQtyButton() {
        this.decreaseQtyButton.click();
    }

    public void clickOnTrashCan() {
        this.trashCan.click();
    }

    public void clickOnContinueShoppingButton() {
        this.continueShoppingButton.click();
    }

    public void clickOnCheckoutButton() {
        this.checkoutButton.click();
    }

    public boolean validateIncreaseQtyButtonIsDisplayed() {
        return this.increaseQtyButton.exists() && this.increaseQtyButton.isDisplayed();
    }

    public boolean validateDecreaseQtyButtonIsDisplayed() {
        return this.decreaseQtyButton.exists() && this.decreaseQtyButton.isDisplayed();
    }

    public boolean validateTrashCanButtonIsDisplayed() {
        return this.trashCan.exists() && this.trashCan.isDisplayed();
    }

    public boolean validateCartBadgeIsDisplayed() {
        return this.subPageTitle.exists() && this.cartBadge.isDisplayed();
    }

    public boolean validateCartBadgeIsNotDisplayed() {
        return !cartBadge.exists();
    }
}

