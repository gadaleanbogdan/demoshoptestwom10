package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class SortDropDownMenu {

    private final SelenideElement sortMenu = $(".sort-products-select");
    private final SelenideElement sortByNameAtoZ = $("[value=az]");
    private final SelenideElement sortByNameZtoA = $("[value=za");
    private final SelenideElement sortByPriceLowToHigh = $("[value=lohi");
    private final SelenideElement sortByPriceHighToLow = $("[value=hilo]");

    public void clickOnTheSortMenu() {
        this.sortMenu.click();
    }

    public boolean validateSortByNameAtoZIsDisplayed() {
        return this.sortByNameAtoZ.exists() && this.sortByNameAtoZ.isDisplayed();
    }

    public boolean validateSortByNameZtoAIsDisplayed() {
        return this.sortByNameZtoA.exists() && this.sortByNameZtoA.isDisplayed();
    }

    public boolean validateSortByPriceLowToHighIsDisplayed() {
        return this.sortByPriceLowToHigh.exists() && this.sortByPriceLowToHigh.isDisplayed();
    }

    public boolean validateSortByPriceHighToLowIsDisplayed() {
        return this.sortByPriceHighToLow.exists() && sortByPriceHighToLow.isDisplayed();
    }

}
