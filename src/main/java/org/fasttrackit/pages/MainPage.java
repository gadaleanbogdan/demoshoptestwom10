package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class MainPage extends Page  {
    private final String title = Selenide.title();

    private final Footer footer;

    private Header header;

    private final SelenideElement sortMenu = $(".sort-products-select");
    private final SelenideElement modal = $(".modal-dialog");
    private final SelenideElement subPageTitle = $(".text-muted");

    public MainPage() {
        System.out.println("Constructing Header");
        this.footer = new Footer();
        System.out.println("Constructing Footer");
        this.header = new Header();
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Footer getFooter() {
        return footer;
    }

    public Header getHeader()  {
        return header;
    }

    public String getPageTitle() {
        return title;
    }

    public String getSubPageTitle() {
        return this.subPageTitle.text();
    }

    public boolean validateSortMenuIsDisplayed() {
        return this.sortMenu.exists() && this.sortMenu.isDisplayed();
    }

    public boolean validateModalIsDisplayed() {
        System.out.println("Verify that the modal is displayed on page.");
        return this.modal.exists() && this.modal.isDisplayed();
    }

    public boolean validateModalIsNotDisplayed() {
        System.out.println("1.Validate that modal is not on page. " );
        return !modal.exists();
    }

    public void clickOnTheShoppingCartIcon() {
        this.header.clickOnTheShoppingCartIcon();
    }

    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();

    }

    public void clickOnTheWishlistButton() {
        this.header.clickOnTheWishlistButton();
    }

    public void clickOnTheResetIcon() {
        this.footer.clickOnTheResetIcon();
    }

    public void clickOnTheLogoButton(){
        this.header.clickOnTheLogoButton();
    }

    public void logUserOut() {
        this.header.clickOnTheLogoutButton();
        sleep(50);
    }
}
