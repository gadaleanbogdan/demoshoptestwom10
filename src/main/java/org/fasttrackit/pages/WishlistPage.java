package org.fasttrackit.pages;


import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class WishlistPage extends MainPage{
    //private final SelenideElement wishlistTitle =
    private final SelenideElement addToCartButton = $(".fa-cart-plus");
    private final SelenideElement removeFromWishlist = $(".fa-heart-broken");


    public void clickOnTheAddToCartButton() {
        this.addToCartButton.click();
    }

    public void clickOnTheRemoveFromWishlist() {
        this.removeFromWishlist.click();
    }


    public boolean validateAddToCartButtonIsDisplayed() {
        return this.addToCartButton.exists() && this.addToCartButton.isDisplayed();
    }
    public boolean validateRemoveFromWishlistIsDisplayed() {
            return this.removeFromWishlist.exists() && this.removeFromWishlist.isDisplayed();
    }

    public void validateThatTheWishlistPageIsDisplayed() {
    }

}
