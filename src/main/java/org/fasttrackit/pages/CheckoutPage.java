package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage {
    private final SelenideElement firstNameField = $("#first-name");
    private final SelenideElement lasNameField = $("#last-name");
    private final SelenideElement addressField = $("#address");
    private final SelenideElement continueCheckoutButton = $(".btn-success");
    private final SelenideElement blankFieldError = $(".error");

    public boolean getBlankFieldError() {
        return blankFieldError.exists() && blankFieldError.isDisplayed();
    }

    public void typeInFirstNameField(String FirstNameToType){
        this.firstNameField.click();
        this.firstNameField.sendKeys(FirstNameToType);
    }
    public void TypeInLastNameField(String LastNameToType){
        this.lasNameField.click();
        this.lasNameField.sendKeys(LastNameToType);
    }
    public void TypeInAddressField(String AddressToType){
        this.addressField.click();
        this.addressField.sendKeys(AddressToType);
    }

    public void clickOnContinueCheckoutButton() {
        this.continueCheckoutButton.click();
    }

    public boolean validateFirstNameFieldIsDisplayed() {
        return this.firstNameField.exists() && this.firstNameField.isDisplayed();
    }
    public boolean validateLastNameFieldIsDisplayed() {
        return this.lasNameField.exists() && this.lasNameField.isDisplayed();
    }
    public boolean validateAddressFieldIsDisplayed() {
        return this.addressField.exists() && this.addressField.isDisplayed();
    }
}
