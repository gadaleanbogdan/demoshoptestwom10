package org.fasttrackit;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class id04_LoginTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @AfterMethod
    public void cleanup() {
        homePage.logUserOut();
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Login Test with valid user",
            dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class)
    public void ad04a_user_can_login_on_the_website(Account account){
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        Header header = new Header(account.getUsername());
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed");
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Expected greetings message to contain the account user");
    }
}
