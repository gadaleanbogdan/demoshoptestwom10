package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ShoppingCartPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class id06_ShoppingCartPageTest extends TestConfiguration {

    MainPage homePage = new MainPage();
    ShoppingCartPage cart = new ShoppingCartPage();

    @BeforeTest
        public void user_can_click_on_a_product(){
            Product p1 = new Product("1");
            p1.addProductToBasket();
    }



    @Test
    public void id06a_shopping_cart_and_elements_are_displayed() {
        homePage.clickOnTheShoppingCartIcon();
        assertEquals(cart.getSubPageTitle(), "Your cart", "Expected Shopping cart title to be Your cart");
        assertTrue(cart.validateIncreaseQtyButtonIsDisplayed(), "Expected increase quantity button to be displayed");
        assertTrue(cart.validateDecreaseQtyButtonIsDisplayed(), "Expected decrease quantity button to be displayed");
        assertTrue(cart.validateTrashCanButtonIsDisplayed(), "Expected trash can button to be displayed");
        cart.clickOnContinueShoppingButton();
    }


    @Test
    public void id06b_shopping_cart_badge_qty() {
        ShoppingCartPage cart = new ShoppingCartPage();
        assertEquals(cart.getCartBadgeQty(), "4");
    }

    @Test
    public void id06c_verifyIfUserCanIncreaseQuantity() {
        homePage.clickOnTheShoppingCartIcon();
        cart.clickOnIncreaseQtyButton();
        assertEquals(cart.getCartBadgeQty(), "5");
        cart.clickOnContinueShoppingButton();
    }

    @Test
    public void id06d_verifyUserCanDecreaseQuantity() {
        homePage.clickOnTheShoppingCartIcon();
        cart.clickOnDecreaseQtyButton();
        assertEquals(cart.getCartBadgeQty(), "4");
        cart.clickOnContinueShoppingButton();
    }

    @Test
    public void id06e_verify_user_can_remove_items_from_cart() {
        homePage.clickOnTheShoppingCartIcon();
        cart.clickOnTrashCan();
        assertEquals(cart.getCartBadgeQty(), "3");
        cart.clickOnContinueShoppingButton();
    }
}
