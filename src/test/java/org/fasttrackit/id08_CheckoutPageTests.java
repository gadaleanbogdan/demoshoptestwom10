package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.CheckoutPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ShoppingCartPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class id08_CheckoutPageTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    ShoppingCartPage cartPage = new ShoppingCartPage();
    CheckoutPage checkout = new CheckoutPage();
    Product p1 = new Product("1");

    @Test
    public void id08a_checkout_page_elements_are_displayed() {
        Product p1 = new Product("1");
        p1.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        cartPage.clickOnCheckoutButton();
        assertTrue(checkout.validateFirstNameFieldIsDisplayed());
        assertTrue(checkout.validateLastNameFieldIsDisplayed());
        assertTrue(checkout.validateAddressFieldIsDisplayed());
        homePage.clickOnTheLogoButton();
    }

    @Test
    public void id08b_user_can_continue_checkout_after_completing_address_information_fields() {
        p1.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        cartPage.clickOnCheckoutButton();
        String firstName = "FirstOne";
        String lastName = "LastOne";
        String address = "CJ";
        checkout.typeInFirstNameField(firstName);
        checkout.TypeInLastNameField(lastName);
        checkout.TypeInAddressField(address);
        checkout.clickOnContinueCheckoutButton();
        assertEquals(homePage.getSubPageTitle(), "Order summary");
        homePage.clickOnTheLogoButton();
    }

    @Test
    public void id08c_user_cant_continue_checkout_without_completing_address_information_fields() {
        p1.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        cartPage.clickOnCheckoutButton();
        checkout.clickOnContinueCheckoutButton();
        assertTrue(checkout.getBlankFieldError());
        homePage.clickOnTheLogoButton();
    }

    @Test
    public void id08d_user_cant_continue_checkout_without_completing_first_name_field() {
        p1.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        cartPage.clickOnCheckoutButton();
        String lastName = "LastOne";
        String address = "CJ";
        checkout.TypeInLastNameField(lastName);
        checkout.TypeInAddressField(address);
        checkout.clickOnContinueCheckoutButton();
        assertTrue(checkout.getBlankFieldError());
        homePage.clickOnTheLogoButton();
    }

    @Test
    public void id08e_user_cant_continue_checkout_without_completing_last_name_field() {
        p1.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        cartPage.clickOnCheckoutButton();
        String firstName = "FirstOne";
        String address = "CJ";
        checkout.typeInFirstNameField(firstName);
        checkout.TypeInAddressField(address);
        checkout.clickOnContinueCheckoutButton();
        assertTrue(checkout.getBlankFieldError());
        homePage.clickOnTheLogoButton();
    }

    @Test
    public void id08f_user_cant_continue_checkout_without_completing_address_field() {
        p1.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        cartPage.clickOnCheckoutButton();
        String firstName = "FirstOne";
        String lastName = "LastOne";
        checkout.typeInFirstNameField(firstName);
        checkout.TypeInLastNameField(lastName);
        checkout.clickOnContinueCheckoutButton();
        assertTrue(checkout.getBlankFieldError());
        homePage.clickOnTheLogoButton();
    }
}
