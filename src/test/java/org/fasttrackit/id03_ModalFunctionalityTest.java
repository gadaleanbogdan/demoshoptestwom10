package org.fasttrackit;

import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class id03_ModalFunctionalityTest extends TestConfiguration {

    MainPage homePage = new MainPage();


    @Test
    public void id03a_modal_Components_Are_Displayed_And_Modal_Can_Be_Closed_Test(){
        homePage.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal is displayed");
        Modal modal = new Modal();
        assertEquals(modal.getModalTitle(), "Login", "Expected Modal title to be Login");
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected Close Button to be displayed");
        assertTrue(modal.validateUsernameFieldIsDisplayed(), "Expected Username field to be displayed");
        assertTrue(modal.validatePasswordFieldIsDisplayed(), "Expected Password field to be displayed");
        modal.clickOnCloseButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed");
    }

    @Test
    public void id03b_username_and_password_fields_are_enabled_and_modal_can_be_closed() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        assertTrue(modal.validateUsernameFieldIsEnabled());
        assertTrue(modal.validatePasswordFieldIsEnabled());
        modal.clickOnCloseButton();
    }
}
