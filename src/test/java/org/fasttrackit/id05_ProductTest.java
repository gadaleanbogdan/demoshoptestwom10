package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.Test;

public class id05_ProductTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @Test(
            dataProvider = "productsDatProvider",
            dataProviderClass = ProductDataProvider.class
    )
    public void id05a_user_can_add_products_to_cart(Product p){
        //sleep(500);
        p.addProductToBasket();

    }

}
