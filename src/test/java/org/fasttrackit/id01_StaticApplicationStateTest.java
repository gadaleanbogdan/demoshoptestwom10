package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.testng.Assert.*;

public class id01_StaticApplicationStateTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @Test
     public void id01a_verifyDemoShopAppTitle() {
        String appTitle = homePage.getPageTitle();
        assertEquals(appTitle, "Demo shop", "Application title is expected to be Demo shop.");

    }

    @Test
    public void id01b_verifyDemoShopFooterBuildDateDetails() {
        String footerDetails = homePage.getFooter().getDetails();
        assertEquals(footerDetails, "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Demo Shop | build date 2021-05-21 14:04:30 GTBDT");
    }

    @Test
     public void id01c_verifyDemoShopContainsQuestionIcon() {
        SelenideElement questionIcon = homePage.getFooter().getQuestionIcon();
        assertTrue(questionIcon.exists(), "Expected Question Icon to exist on Page");
        assertTrue(questionIcon.isDisplayed(), "Expected Question Icon to be displayed");
    }

    @Test
     public void id01d_verifyDemoShopFooterContainsResetIcon() {
        SelenideElement resetIconTitle = homePage.getFooter().getResetIconTitle();
        assertTrue(resetIconTitle.exists(), "Expected Reset Icon to exist on Page");
        assertTrue(resetIconTitle.isDisplayed(), "Expected Reset Icon to be displayed");
    }

}