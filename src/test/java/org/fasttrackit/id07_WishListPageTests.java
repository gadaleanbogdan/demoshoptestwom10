package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.WishlistPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class id07_WishListPageTests extends TestConfiguration {

    MainPage homePage = new MainPage();


    @Test
    public void id07a_wishlist_page_and_page_elements_are_displayed() {
        Product p1 = new Product("1");
        p1.addToFavorite();
        homePage.clickOnTheWishlistButton();
        WishlistPage wishlist = new WishlistPage();
        assertTrue(wishlist.validateRemoveFromWishlistIsDisplayed());
        assertTrue(wishlist.validateAddToCartButtonIsDisplayed());
        homePage.clickOnTheLogoButton();
    }

}
