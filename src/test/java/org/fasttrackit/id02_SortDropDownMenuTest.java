package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.SortDropDownMenu;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class id02_SortDropDownMenuTest extends TestConfiguration {
    MainPage homePage = new MainPage();

    @Test
    public void  id02a_verifyDropDownMenuOptions() {
        SortDropDownMenu sortDropDownMenu = new SortDropDownMenu();
        sortDropDownMenu.clickOnTheSortMenu();
        assertTrue(homePage.validateSortMenuIsDisplayed(), "Expected sort menu to be displayed.");
        assertTrue(sortDropDownMenu.validateSortByNameAtoZIsDisplayed(), "Expected Sort by name (A to Z) to be displayed. ");
        assertTrue(sortDropDownMenu.validateSortByNameZtoAIsDisplayed(),"Expected Sort by name (Z to A) to be displayed.");
        assertTrue(sortDropDownMenu.validateSortByPriceLowToHighIsDisplayed(),"Expected Sort by price (Low to High) to be displayed.");
        assertTrue(sortDropDownMenu.validateSortByPriceHighToLowIsDisplayed(),"Expected Sort by price (High to Low) to be displayed.");
        sleep(1600);
    }
}
