package org.fasttrackit.dataprovider;

import org.fasttrackit.products.Product;
import org.testng.annotations.DataProvider;

public class ProductDataProvider {

    @DataProvider(name = "productsDatProvider")
    public static Object[][] createProductsProvider(){
        Product p1 = new Product("1");
        Product p2 = new Product("2");
        Product p3 = new Product("3");
        Product p4 = new Product("4");
        return new Object[][] {
                {p1},
                {p2},
                {p3},
                {p4}
        };
    }
}
