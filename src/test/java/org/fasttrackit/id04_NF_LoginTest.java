package org.fasttrackit;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class id04_NF_LoginTest extends TestConfiguration {

    MainPage homePage = new MainPage();
    Modal modal = new Modal();


    @AfterMethod
    public void cleanup() {
        modal.clickOnCloseButton();
        homePage.clickOnTheResetIcon();
    }

    @Test
    public void id04b_user_cant_login_on_page_without_password_test(){
        homePage.clickOnTheLoginButton();
        String user = "beetle";
        modal.typeInUsernameField(user);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to be closed");
    }

    @Test
    public void is04c_user_cant_login_on_page_without_username_test(){
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String password = "choochoo";
        modal.typeInPasswordField(password);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to be closed");
    }

    @Test
    public void id04d_user_cant_login_on_page_with_invalid_password_test(){
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String username = "beetle";
        String password = "moomoo";
        modal.typeInUsernameField(username);
        modal.typeInPasswordField(password);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to be closed");
    }

    @Test
    public void id04e_user_cant_login_on_page_with_invalid_username_test(){
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String username = "bital";
        String password = "choochoo";
        modal.typeInUsernameField(username);
        modal.typeInPasswordField(password);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to be closed");
    }
}
